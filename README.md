# Application Library pour la gestion des emprunts de livres entièrement Kubernetisée

Prérequis :
  - Avoir un cluster Kubernetes fonctionnel
  - Etre capable d'attaquer ce cluster sur votre machine à l'aide de la commande kubectl

Une fois le projet cloné :

   1- Se déplacer dans le repertoire /kube-iac-database et saisir la commande suivante dans un Terminal pour créer le pod et les ressources dédiées à la base de données MySQL de l'application : **kubectl apply -k .** 
   
   3- Se déplacer dans le repertoire /kube-iac-app et saisir la commande suivante dans un Terminal pour créer les ressources front et back de l'application : **kubectl apply -k .** 

L'application est accessible ici : http://library.gkemayo.com/annuaire-ui/#/
